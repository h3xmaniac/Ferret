#!/usr/bin/python3
import socket
import sys
import os
import platform
import time
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
animation = '''|/-\\'''
idx = 0

logo = '''                                        
               _....._            ,_    _,
             .-'       '.       _ /7 \--/^|
           .'            `'._.-' ;   -   _|
         _/                      '. /O\ /o/
      .-' |                '       '-._Y.'
     /__.'\  '-.         .'           /
    |__/   '-._ '-.,___,.     ,   _.-L
    \__\       `""`      `-._  \-'.__,\\
     '-.;        Ferret     '-._'.  ```                                                         
     	  Tiny socket listener
\n\n'''
print(logo)
host = str(input("> Which host we connecting?	"))
port = int(input("> Port tho?	"))
server.bind( ( host, port ) )
server.listen( 5 )
connected = False
while connected == False:
	print(f"\n >>  Server bound to {host}:{port}  << \n\n")
	print("Forever listening for a shell...",animation[idx % len(animation)] + "\r",)
	idx += 1
	time.sleep(0.1)
	os.system('clear')
while 1:
	if not connected:
		(client, address) = server.accept()
		connected = True
print(f"--> Accepted connection from {host}")
buffer = ""
while connected:
	try:
		recv_buffer = client.recv(4096)
		print(f"[!!] Received: {recv_buffer}")
		if not len(recv_buffer):
			break
		else:
			buffer += recv_buffer
	except:
		break
command = input("[#]Enter command>	")
client.sendall( command + "\r\n\r\n")
print(f"--> Sent => {command}")
